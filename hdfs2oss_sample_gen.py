# -*- coding: utf-8 -*-

import tensorflow as tf
import collections
import sys
import os
from tensorflow.python.platform import gfile

global features_spec_dict

def features_spec_gen(feature_only = False):
  if not os.path.exists('./features_spec_file'):
    raise ValueError('features_spec_file not detected.')
  features_spec = collections.OrderedDict()
  if not feature_only:
    features_spec['click'] = tf.FixedLenFeature([1], tf.float32)
    features_spec['adq'] = tf.FixedLenFeature([1], tf.float32)
  try:
    readGFile = gfile.GFile('./features_spec_file', mode='r')
    for line in readGFile.readlines():
      line = line.strip()
      if line.startswith('#'):
        continue
      feature_column_name, feature_column_idx = line.split('\t')
      feature_column_attribute = 'CategoryFeature'
      feature_column_name = feature_column_name.strip()
      feature_column_idx = feature_column_idx.strip()
      spec_key = int(feature_column_idx) if feature_only else feature_column_idx
      spec_val = tf.VarLenFeature(tf.int64) if feature_column_attribute == 'CategoryFeature' \
                                            else tf.VarLenFeature(tf.float32)
      if features_spec.has_key(spec_key):
        raise ValueError('duplicate spec_key, pls check.')
      features_spec[spec_key] = spec_val
  except:
    pass
  return features_spec

def get_tfrecords_example(raw_sample):
  tfrecords_features = {}
  for k, v in features_spec_dict.items():
    if k in {'adq', 'click'}:
      tfrecords_features[k] = tf.train.Feature(float_list=tf.train.FloatList(value=raw_sample[k]))
    else:
      fea_slot = k
      if raw_sample.has_key(fea_slot):
        tfrecords_features[fea_slot] = \
          tf.train.Feature(int64_list=tf.train.Int64List(value=raw_sample[fea_slot]))
      else:
        tfrecords_features[fea_slot] = tf.train.Feature(int64_list=tf.train.Int64List(value=[0]))
  return tf.train.Example(features=tf.train.Features(feature=tfrecords_features))

features_spec_dict = features_spec_gen()

input_file_path = os.environ['map_input_file']
part_number = os.path.split(input_file_path).split('-')
output_file = './tfrecord-' + part_number

tfrecord_wrt = tf.python_io.TFRecordWriter(output_file)
for line in sys.stdin:
  line = line.rstrip('\n')
  if not line:
    pass
  line = line.split(' ')
  raw_sample = {}
  for idx in range(len(line)):
    if idx == 0:
      raw_sample['adq'] = [float(line[idx])]
    elif idx == 1:
      continue
    elif idx == 2:
      raw_sample['click'] = [float(line[idx])]
    else:
      fea_idx, fea_slot = line[idx].split(':')
      if raw_sample.has_key(fea_slot):
        raw_sample[fea_slot].append(int(fea_idx))
      else:
        raw_sample[fea_slot] = [int(fea_idx)]
  exmp = get_tfrecords_example(raw_sample)
  tfrecord_wrt.write(exmp.SerializeToString())

