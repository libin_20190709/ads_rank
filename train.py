# encoding:utf-8

import math
import tensorflow as tf
import numpy as np
import datetime
import time
import os
from sklearn import metrics
from flag_define import *
from data import *
from model import *

config = tf.ConfigProto(log_device_placement = True,
                        inter_op_parallelism_threads = 32, intra_op_parallelism_threads = 32)

def echo_flags():

  print('checkpointDir is: ', FLAGS.checkpointDir)
  print('output is: ', FLAGS.outputs)
  print('mode is: ', FLAGS.mode)
  print('save_checkpoint_steps is: ', FLAGS.save_checkpoint_steps)

  print('batch size is: ', FLAGS.batch_size)
  print('train epoch is :', FLAGS.train_epoch)
  print('test epoch is: ', FLAGS.test_epoch)
  print('feature_column_num is: ', FLAGS.feature_column_num)
  print('embedding_dim is: ', FLAGS.embedding_dim)
  print('use_deep is: ', FLAGS.use_deep)
  print('deep_optimizer_type is: ', FLAGS.deep_optimizer_type)
  print('d_init_learning_rate is: ', FLAGS.d_init_learning_rate)
  print('use_wide is: ', FLAGS.use_wide)
  print('wide_optimizer_type is: ', FLAGS.wide_optimizer_type)
  print('w_init_learning_rate is: ', FLAGS.w_init_learning_rate)
  print('steps_per_decay is: ', FLAGS.steps_per_decay)
  print('do_bn is: ', FLAGS.do_bn)

def do_eval(labels, preds):
  eval_metrics_set = FLAGS.eval_metrics.split(',')
  len_eval_metrics_set = len(eval_metrics_set)
  metrics_dict = {}
  for idx in range(len_eval_metrics_set):
    if eval_metrics_set[idx] == 'auc':
      fpr, tpr, thresholds = metrics.roc_curve(labels, preds, pos_label = 1)
      eval_auc = metrics.auc(fpr, tpr)
      metrics_dict['auc'] = eval_auc
    elif eval_metrics_set[idx] == 'mse':
      eval_mse = metrics.mean_squared_error(labels, preds)
      metrics_dict['mse'] = eval_mse
    elif eval_metrics_set[idx] == 'logloss':
      eval_logloss = metrics.log_loss(labels, preds)
      metrics_dict['logloss'] = eval_logloss
  return metrics_dict

def get_time_stamp():
  ct = time.time()
  local_time = time.localtime(ct)
  data_head = time.strftime("%Y-%m-%d %H:%M:%S", local_time)
  data_secs = (ct - int(ct)) * 1000
  time_stamp = "%s.%03d" % (data_head, data_secs)
  return time_stamp

def main(unused_argv):

  if FLAGS.job_name is None or FLAGS.job_name == '':
    raise ValueError('Must specify an explicit job_name !')
  else:
    print 'job_name : %s' % FLAGS.job_name
  if FLAGS.task_index is None or FLAGS.task_index == '':
    raise ValueError('Must specify an explicit task_index!')
  else:
    print 'task_index : %d' % FLAGS.task_index

  ps_spec = FLAGS.ps_hosts.split(',')
  worker_spec = FLAGS.worker_hosts.split(',')

  worker_device = "/job:worker/task:%d/cpu:%d" % (FLAGS.task_index, 0)
  available_worker_device = "/job:worker/task:%d" % (FLAGS.task_index)

  cluster = tf.train.ClusterSpec({'ps': ps_spec, 'worker': worker_spec})
  server = tf.train.Server(cluster, job_name = FLAGS.job_name,
                           task_index = FLAGS.task_index, protocol = "grpc")
  if FLAGS.job_name == 'ps':
    server.join()

  with tf.device(worker_device):

    echo_flags()

    print('Start to Make Files_Name_Pattern !')
    files_name_pattern = FLAGS.tables.split(',')
    train_files_name_pattern, test_files_name_pattern = files_name_pattern[:-1], [files_name_pattern[-1]]
    print('Train_Files_Name_Pattern is: ', train_files_name_pattern)
    print('Test_Files_Name_Pattern is: ', test_files_name_pattern)
    print('Make Files_Name_Pattern Done !')
   
    if FLAGS.mode == 'train':
      print('Start Loading Train Data !')
      train_features, train_labels = input_fn(train_files_name_pattern, tf.estimator.ModeKeys.TRAIN, len(worker_spec), FLAGS.task_index, FLAGS.batch_size, FLAGS.train_epoch, FLAGS.feature_column_num)
      print('Loading Train Data Done !')

    if FLAGS.mode == 'test':
      print('Start Loading Test Data !')
      test_features, test_labels = input_fn(test_files_name_pattern, tf.estimator.ModeKeys.EVAL, 1, 0, FLAGS.batch_size, FLAGS.test_epoch, FLAGS.feature_column_num)
      pred_set = []
      lable_set = []
      print('Loading Test Data Done !')
  
  is_chief = (FLAGS.task_index == 0)
  with tf.device(tf.train.replica_device_setter(worker_device = available_worker_device, cluster = cluster)):

    global_step = tf.Variable(0, trainable = False, name = 'global_step')

    if FLAGS.mode == 'train':
      print('Start Building Train Model !')
      group_train_op, g_step, loss, learning_rate = model_fn(train_features, train_labels, tf.estimator.ModeKeys.TRAIN)
      save_checkpoint_steps = FLAGS.save_checkpoint_steps
      print('Building Train Model Done !')

    if FLAGS.mode == 'test':
      print('Start Building Test Model !')
      pred, label = model_fn(test_features, test_labels, tf.estimator.ModeKeys.EVAL)
      save_checkpoint_steps = None
      print('Building Test Model Done !')

    with tf.train.MonitoredTrainingSession(master = server.target, is_chief = is_chief,
                                           checkpoint_dir = FLAGS.checkpointDir,
                                           save_checkpoint_steps = save_checkpoint_steps,
                                           config = config, log_step_count_steps = 100) as sess:
      while not sess.should_stop():
        if FLAGS.mode == 'train':
          if is_chief:
            g_step_ = sess.run(global_step)
            if g_step_ == 0:
              print('Start Training Model !')
          _, g_step_, loss_, lr_ = sess.run([group_train_op, g_step, loss, learning_rate])
          print("\nTime: %s | Train Loss: %-4.7f | Global_Step: %-1i | Learning_Rate : %-4.8f"
                %(get_time_stamp(), loss_, g_step_, lr_))
        if FLAGS.mode == 'test':
          if is_chief:
            print('Start Testing Model !')
            step = 1
            try:
              while True:
                pred_, label_ = sess.run([pred, label])
                print(step)
                step += 1
                pred_set.append(pred_)
                lable_set.append(label_)
            except tf.errors.OutOfRangeError:
              print('Eval Data has been consumed !')
              break
      if FLAGS.mode == 'train':
        print('Finish Training Model !')
      elif FLAGS.mode == 'test':
        flattened_pred_set = np.reshape(np.vstack(pred_set), -1)
        flattened_lable_set = np.reshape(np.vstack(lable_set), -1)
        print(flattened_pred_set)
        print(flattened_lable_set)
        print('start to do metrics_eval !')
        metrics_dict = do_eval(flattened_lable_set, flattened_pred_set)
        print('metrics_dict is: ', metrics_dict)
        writer = tf.python_io.TableWriter(FLAGS.outputs)
        records = writer.write(zip(flattened_lable_set, flattened_pred_set), indices=[0, 1])
        writer.close()
        print('Finish Testing Model !')

if __name__ == '__main__':
  tf.app.run()

