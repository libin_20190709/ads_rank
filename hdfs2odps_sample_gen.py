#!/app/opt/cloudera/parcels/Anaconda/bin/python

import collections
import sys
import os

def features_spec_gen(feature_only = False):
  if not os.path.exists('./features_spec_file'):
    raise ValueError('features_spec_file not detected.')
  features_spec = collections.OrderedDict()
  if not feature_only:
    features_spec['click'] = True
    features_spec['adq'] = True
  try:
    f = open('./features_spec_file','r')
    for line in f.readlines():
      line = line.strip()
      if line.startswith('#'):
        continue
      feature_column_name, feature_column_idx = line.split('\t')
      feature_column_attribute = 'CategoryFeature'
      feature_column_name = feature_column_name.strip()
      feature_column_idx = feature_column_idx.strip()
      spec_key = int(feature_column_idx) if feature_only else feature_column_idx
      spec_val = True
      if features_spec.has_key(spec_key):
        raise ValueError('duplicate spec_key, pls check.')
      features_spec[spec_key] = spec_val
  except:
    pass
  return features_spec

def get_tablerecords_example(raw_sample, features_spec_dict):
  output = ''
  for k, v in features_spec_dict.items():
    if k in {'adq', 'click'}:
      if k == 'click':
        if raw_sample.has_key(k):
          output = ''.join(str(e) for e in raw_sample[k])
        else:
          break
    else:
      fea_slot = k
      if raw_sample.has_key(fea_slot):
        sorted_fea_slot = sorted(raw_sample[fea_slot])
        output = output + ' ' + ','.join(str(e) for e in raw_sample[fea_slot])
        #output = output + ' ' + ','.join(str(e) + ':' + fea_slot for e in sorted_fea_slot)
      else:
        output = output + ' ' + '0'
        #output = output + ' ' + '0' + ':' + fea_slot
  return output 

def main():
  
  features_spec_dict = features_spec_gen()
  for line in sys.stdin:
    line = line.strip()
    if not line:
      pass
    line = line.split(' ')
    raw_sample = {}
    try:
      for idx in range(len(line)):
        if idx == 0:
          raw_sample['adq'] = [line[idx]]
        elif idx == 1:
          continue
        elif idx == 2:
          raw_sample['click'] = [float(line[idx])]
        else:
          if line[idx].find('user_vector') != -1 or line[idx].find('dnn_input') != -1:
            continue
          fea_info, hit_or_miss = line[idx].split('-')
          if hit_or_miss == 'hit':
            fea_idx, fea_slot = fea_info.split(':')
            if raw_sample.has_key(fea_slot):
              raw_sample[fea_slot].append(int(fea_idx))
            else:
              raw_sample[fea_slot] = [int(fea_idx)]
    except:
      pass
    if len(raw_sample) != 0:
      exmp = get_tablerecords_example(raw_sample, features_spec_dict)
      if len(exmp.split(' ')) == len(features_spec_dict) - 1:
        print exmp

if __name__ == "__main__":

  main()
