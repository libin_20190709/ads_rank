# encoding:utf-8

import tensorflow as tf
import multiprocessing
import collections
import os
import sys

features_spec_file = './features_spec_file'

def features_spec_gen(feature_only = False):
  if not os.path.exists(features_spec_file):
    raise ValueError('features_spec_file not detected.')
  features_spec = collections.OrderedDict()
  if not feature_only:
    features_spec['click'] = tf.FixedLenFeature([1], tf.float32)
    features_spec['adq'] = tf.FixedLenFeature([1], tf.float32)
  try:
    f = open(features_spec_file, 'r')
    for line in f.readlines():
      line = line.strip()
      if line.startswith('#'):
        continue
      #feature_column_name, feature_column_idx, feature_column_attribute = line.split('\t')
      feature_column_name, feature_column_idx = line.split('\t')
      feature_column_attribute = 'CategoryFeature'
      feature_column_name = feature_column_name.strip()
      feature_column_idx = feature_column_idx.strip()
      spec_key = int(feature_column_idx) if feature_only else feature_column_idx
      spec_val = tf.VarLenFeature(tf.int64) if feature_column_attribute == 'CategoryFeature' \
                                            else tf.VarLenFeature(tf.float32)
      if features_spec.has_key(spec_key):
        raise ValueError('duplicate spec_key, pls check.')
      features_spec[spec_key] = spec_val
  except:
    pass
  print('features_spec_gen finished !')
  return features_spec

def features_spec_gen_table(feature_only = False):
  if not os.path.exists(features_spec_file):
    raise ValueError('features_spec_file not detected.')
  features_spec = collections.OrderedDict()
  if not feature_only:
    features_spec['click'] = tf.FixedLenFeature([1], tf.float32)
    features_spec['adq'] = tf.FixedLenFeature([1], tf.float32)
  count = 1
  try:
    f = open(features_spec_file, 'r')
    for line in f.readlines():
      line = line.strip()
      if line.startswith('#'):
        continue
      #feature_column_name, feature_column_idx, feature_column_attribute = line.split('\t')
      feature_column_name, feature_column_idx = line.split('\t')
      feature_column_attribute = 'CategoryFeature'
      feature_column_name = feature_column_name.strip()
      feature_column_idx = feature_column_idx.strip()
      spec_key = str(count)
      spec_val = tf.VarLenFeature(tf.int64) if feature_column_attribute == 'CategoryFeature' \
                                            else tf.VarLenFeature(tf.float32)
      if features_spec.has_key(spec_key):
        raise ValueError('duplicate spec_key, pls check.')
      features_spec[spec_key] = spec_val
      count += 1
  except:
    pass
  print('features_spec_gen_table finished !')
  return features_spec

def parse_exmp(serialized_ins):
  raw_sample = tf.parse_single_example(serialized_ins, features = features_spec_gen())
  features = {}
  for k, v in raw_sample.items():
    if k != 'click':
      features[k] = v
  labels = raw_sample['click']
  return features, labels

def input_fn_from_tfrecord(files_name_pattern, mode, batch_size, num_epochs):
  shuffle = True if mode == tf.estimator.ModeKeys.TRAIN else False
  num_threads = multiprocessing.cpu_count() / 4 if False else 1
  file_names = tf.matching_files(files_name_pattern)
  dataset = tf.data.TFRecordDataset(file_names)
  dataset = dataset.map(map_func = parse_exmp, num_parallel_calls = num_threads)
  dataset = dataset.batch(batch_size)
  dataset = dataset.prefetch(buffer_size = batch_size * 50)
  if shuffle:
    dataset = dataset.shuffle(batch_size)
  dataset = dataset.repeat(num_epochs)
  iterator = dataset.make_one_shot_iterator()
  features, labels = iterator.get_next()
  labels = tf.cast(labels, tf.int64)
  return features, labels

def input_fn_from_table(files_name_pattern, mode,
                        slice_count, slice_id, batch_size, num_epochs, feature_column_num):
  min_after_dequeue = 1000
  file_name_queue = tf.train.string_input_producer(files_name_pattern,
                                                   num_epochs = num_epochs, shuffle = False)
  reader = tf.TableRecordReader(slice_count = slice_count, slice_id = slice_id,
                                csv_delimiter = ';', capacity = 120 * batch_size, num_threads = 24)
  key, value = reader.read_up_to(file_name_queue, batch_size)
  capacity = min_after_dequeue + 15 * batch_size
  if mode == tf.estimator.ModeKeys.TRAIN:
    batched_value = tf.train.shuffle_batch([value], batch_size = batch_size,
              capacity = capacity, min_after_dequeue = min_after_dequeue, enqueue_many = True)
  else:
    batched_value = tf.train.batch([value], batch_size = batch_size, capacity = capacity, enqueue_many = True)
  records = tf.decode_csv(batched_value,
                          record_defaults = [[0.0]] * 1 + [['']] * feature_column_num, field_delim = ';')
  labels = tf.cast(records[0], tf.int64)
  features = {}
  for i in range(1, len(records)):
    batched_sparse_col_string = tf.string_split(records[i], delimiter = ',')
    batched_sparse_col_int_val = \
      tf.string_to_hash_bucket_fast(batched_sparse_col_string.values, num_buckets = sys.maxint)
    features[str(i)] = tf.SparseTensor(indices = batched_sparse_col_string.indices,
                                       values = batched_sparse_col_int_val,
                                       dense_shape = batched_sparse_col_string.dense_shape)
  return features, labels

def input_fn(files_name_pattern,
             mode,
             slice_count,
             slice_id,
             batch_size,
             num_epochs,
             feature_column_num,
             data_source_type = 'table'
             ):
  if not data_source_type in {'table', 'oss'}:
    raise ValueError('invalid data_source_type.')
  if data_source_type == 'table':
    return input_fn_from_table(files_name_pattern, mode,
                               slice_count, slice_id, batch_size, num_epochs, feature_column_num)
  else:
    return input_fn_from_tfrecord(files_name_pattern, mode, batch_size, num_epochs)
  
