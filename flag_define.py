# encoding:utf-8

import tensorflow as tf

tf.app.flags.DEFINE_string("buckets", "", "buckets info")
tf.app.flags.DEFINE_string("tables", "", "input tables info")
tf.app.flags.DEFINE_string("output_tables", "", "output tables info")
tf.app.flags.DEFINE_string("checkpointDir", "", "checkpointDir info")
tf.app.flags.DEFINE_string("outputs", "", "save preds val in outputs table")
tf.app.flags.DEFINE_string("ps_hosts", "", "ps_hosts")
tf.app.flags.DEFINE_string("worker_hosts", "", "worker_hosts")
tf.app.flags.DEFINE_string("job_name", "", "job_name")
tf.app.flags.DEFINE_integer("task_index", 0, "task_index")
tf.app.flags.DEFINE_integer("save_checkpoint_steps", 10000, "task_index")

tf.app.flags.DEFINE_integer("batch_size", 128, "batch size of process")
tf.app.flags.DEFINE_integer("train_epoch", 1, "train epoch number")
tf.app.flags.DEFINE_integer("test_epoch", 1, "test epoch number")
tf.app.flags.DEFINE_integer("feature_column_num", 150, "number of feature column")
tf.app.flags.DEFINE_string("mode", "", "running mode")
tf.app.flags.DEFINE_string("eval_metrics", "", "eval_metric needed")
tf.app.flags.DEFINE_integer("embedding_dim", 14, "dimension of embedding")
tf.app.flags.DEFINE_boolean("use_features_of_all_type", False, "use category and continuous features both")
tf.app.flags.DEFINE_boolean("use_features_of_category_type", True, "use category features only")
tf.app.flags.DEFINE_string("model_type", "wide_and_deep", "model type")
tf.app.flags.DEFINE_boolean("use_deep", True, "use deep-side model")
tf.app.flags.DEFINE_string("deep_optimizer_type", "adagrad", "optimizer of deep-side model")
tf.app.flags.DEFINE_float("d_init_learning_rate", 0.5, "initial learning rate of deep-side model")
tf.app.flags.DEFINE_boolean("use_wide", False, "use wide-side model")
tf.app.flags.DEFINE_string("wide_optimizer_type", "ftrl", "optimizer of wide-side model")
tf.app.flags.DEFINE_float("w_init_learning_rate", 0.5, "initial learning rate of wide-side model")
tf.app.flags.DEFINE_integer("wide_part_order", 1, "funcation approximation order")
tf.app.flags.DEFINE_boolean("use_regularization", False, "use regularization")
tf.app.flags.DEFINE_float("grad_clip_val", 5.0, "gradient clip bound")
tf.app.flags.DEFINE_integer("steps_per_decay", 3000, "decay every n steps")
tf.app.flags.DEFINE_float("decay_factor", 0.999, "decay factor")
tf.app.flags.DEFINE_boolean("do_bn", True, "do batch normalization")
tf.app.flags.DEFINE_boolean("debug", True, "debug to output model parameters information")
tf.app.flags.DEFINE_string("kernel_initializer_type", "he", "kernel initialization type")
tf.app.flags.DEFINE_string("activation_type", "relu", "pay attention that it's best to match the type of kernel_initializer_type in deep mlp network (eg: >= 7 layers)")

FLAGS = tf.app.flags.FLAGS

