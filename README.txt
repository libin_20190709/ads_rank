
****************************************************************

文件备注：

data.py:   ODPS Table 以及 OSS TFRecord 样本读取和格式解析逻辑（基于训练效率， 统一处理成 sparseTensor）

model.py:  model_fn 接口以及 model 计算图定义(目前提供了wide and deep),  返回结果基于任务运行模式不同

train.py:  tf 训练任务控制逻辑， 包括 input_fn，model_fn 抽象接口调用， 变量设备分配， 训练和测试任务调度

flag_define.py: tf 任务启动参数配置集合，包括输入样本地址，模型保存地址，预测结果保存地址;  以及模型本身关联的各类超参数

hdfs2odps_sample_gen.py： 从 HDFS 样本生成 ODPS Tabel 样本逻辑

hdfs2oss_sample_gen.py:   从 HDFS 样本生成 OSS TFRecord 样本逻辑


****************************************************************

运行命令:

1  准备本地压缩包,  cd 到 data.py 所在文件夹， tar -czf xx_name.tar.gz ./*

2  根据需要执行相关任务


train:

pai -name tensorflow180_lite_ev_init -project algo_platform_dev -Dscript="file://本地压缩包路径/xx_name.tar.gz" -DentryFile="./train.py" -Dtables="odps://algo_platform_dev/tables/huichuan_fea_base_test_from_qichao/pt=20190212,odps://algo_platform_dev/tables/huichuan_fea_base_test_from_qichao/pt=2019022720" -Doutputs="odps://algo_platform_dev/tables/output_table_name(u need to establish first)/pt=xx" -DcheckpointDir="oss://dev-imp-data-feature/checkpointdir(u need to establish first)/?role_arn=acs:ram::1274723469870537:role/imp-rank-dev&host=cn-zhangjiakou.oss-internal.aliyun-inc.com" -Dcluster='{\"ps\":{\"count\":15,\"cpu\":1000,\"memory\":9000},\"worker\":{\"count\":128,\"cpu\":500,\"memory\":8000}}' -DenableDynamicCluster=true -DuserDefinedParameters="--batch_size=xx --train_epoch=xx --test_epoch=xx --feature_column_num=xx --mode=train --eval_metrics=auc,mse";


test(-Dcluster 需要和 train 保持一致, 且需要确保 checkpoint 地址下模型是否存在):

pai -name tensorflow180_lite_ev_init -project algo_platform_dev -Dscript="file://本地压缩包路径/xx_name.tar.gz" -DentryFile="./train.py" -Dtables="odps://algo_platform_dev/tables/huichuan_fea_base_test_from_qichao/pt=20190212,odps://algo_platform_dev/tables/huichuan_fea_base_test_from_qichao/pt=2019022720" -Doutputs="odps://algo_platform_dev/tables/output_table_name(u need to establish first)/pt=xx" -DcheckpointDir="oss://dev-imp-data-feature/checkpointdir(u need to establish first)/?role_arn=acs:ram::1274723469870537:role/imp-rank-dev&host=cn-zhangjiakou.oss-internal.aliyun-inc.com" -Dcluster='{\"ps\":{\"count\":15,\"cpu\":1000,\"memory\":9000},\"worker\":{\"count\":128,\"cpu\":500,\"memory\":8000}}' -DenableDynamicCluster=true -DuserDefinedParameters="--batch_size=xx --train_epoch=xx --test_epoch=xx --feature_column_num=xx --mode=test --eval_metrics=auc,mse";


ps: -Dcluster 内 count:128 表示 128个 worker 或者 ps;  cpu: 500 表示 5个物理核;  memory: 8000 表示 8g 内存;


任务进度页面查看(eg, 页面过了保存期，自动清空):

https://logview.alibaba-inc.com/logview/?h=http://service-corp.odps.aliyun-inc.com/api&p=algo_platform_dev&i=20190705094615489gfrn8vzt2_3c8401e6_3822_42a2_ac83_fdfec37881ef&token=OEpOb1NGODF5RVJTaDh5MUd4aWFobXo5MmprPSxPRFBTX09CTzoxMDA0MTU2MTU2MDIxNzM5LDE1NjI5MjQ3NzcseyJTdGF0ZW1lbnQiOlt7IkFjdGlvbiI6WyJvZHBzOlJlYWQiXSwiRWZmZWN0IjoiQWxsb3ciLCJSZXNvdXJjZSI6WyJhY3M6b2RwczoqOnByb2plY3RzL2FsZ29fcGxhdGZvcm1fZGV2L2luc3RhbmNlcy8yMDE5MDcwNTA5NDYxNTQ4OWdmcm44dnp0Ml8zYzg0MDFlNl8zODIyXzQyYTJfYWM4M19mZGZlYzM3ODgxZWYiXX1dLCJWZXJzaW9uIjoiMSJ9

资源消耗页面查看

http://pre-fuxi-sensor.alibaba.net/d/AbG4tPomz/fuxisensor_worker_resource_usage?orgId=1&var-DataSource=ay38a&var-WorkerName=Odps%2Falgo_platform_dev_20190705031535233g3wr8vzt2_c749a167_3101_44eb_8b77_8e528f0aa111_AlgoTask_0_0%2Fps@a68d04439.na62%2312

WorkerName: 内粘贴一个机器地址（从任务进度页面内，选择一个 ps 或者 worker 节点， 点击 Path 按钮即可）


