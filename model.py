# encoding:utf-8

import tensorflow as tf
from tensorflow.python.ops import variable_scope as vs
from tensorflow.contrib.layers.python import layers
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.framework import ops
from tensorflow.python.ops import state_ops
from flag_define import *
from data import *


def get_activation_func(activation_type):
  if activation_type == 'relu':
    activation_func = tf.nn.relu 
  elif activation_type == 'tanh':
    activation_func = tf.nn.tanh
  elif activation_type == 'prelu':
    activation_func = tf.keras.layers.PReLU
  else:
    activation_func = tf.nn.sigmoid
  return activation_func 

def get_regularizer(regularizer_type):
  if regularizer_type == 'l2':
    regularizer = tf.contrib.layers.l2_regularizer(scale = 0.0001)
  elif regularizer_type == 'l1':
    regularizer = tf.contrib.layers.l1_regularizer(scale = 0.0001)
  else:
    regularizer = None
  return regularizer

def get_initializer(initializer_type, uniform = False):
  if initializer_type == 'xavier':
    initializer = tf.contrib.layers.xavier_initializer(uniform)
  elif initializer_type == 'he':
    initializer = tf.keras.initializers.he_normal() if not uniform else tf.keras.initializers.he_uniform()
  elif initializer_type == 'glorot':
    initializer = tf.keras.initializers.glorot_normal()
  elif initializer_type == 'truncated_normal':
    initializer = tf.truncated_normal_initializer(mean = 0.0, stddev = 0.01)
  else:
    initializer = tf.zeros_initializer()
  return initializer

def do_mask(embed_matrix, col_features, is_use = True, default_fea_sign = tf.constant(5975597238397796928, dtype = tf.int64)):
  if not isinstance(col_features, tf.SparseTensor):
    raise ValueError('invalid features type, SparseTensor is needed here.')
  lookup_result = \
    tf.nn.embedding_lookup_sparse(embed_matrix, col_features, None)
    #layers.embedding_lookup_sparse_with_distributed_aggregation(embed_matrix, col_features, None)
  if is_use:
    if default_fea_sign.dtype != tf.int64:
      raise ValueError('invalid default_fea_sign type, tf.int64 is needed here.')
    col_features = tf.sparse_to_dense(col_features.indices, col_features.dense_shape, col_features.values)
    masked_col_features = tf.reduce_any(tf.not_equal(col_features, default_fea_sign), 1)
    lookup_result = tf.multiply(lookup_result, \
                                tf.expand_dims(tf.cast(masked_col_features, dtype = tf.float32), 1))
  return lookup_result

def get_input_layer(features, embed_key_type = tf.int64, wide_used = True, deep_used = True,
                    mode = tf.estimator.ModeKeys.TRAIN):

  features_spec = features_spec_gen_table(True)
  if not features_spec:
    raise ValueError('empty features_spec.')

  input_layer_partitioner = tf.fixed_size_partitioner(15)

  if mode == tf.estimator.ModeKeys.TRAIN:
    emb_initializer = get_initializer('truncated_normal')
  else:
    emb_initializer = get_initializer('zero')

  deep_input = None
  if deep_used:
    with vs.variable_scope('deep_input', partitioner = input_layer_partitioner, reuse = tf.AUTO_REUSE):
      deep_embed_matrix = tf.get_embedding_variable('deep_emb_matrix', embedding_dim = FLAGS.embedding_dim,
                                                    key_dtype = embed_key_type,
                                                    initializer = emb_initializer)
      deep_input_from_raw_category = []
      deep_input_from_raw_dense = []
      for k, _ in features_spec.items():
        k = str(k)
        if _ == tf.VarLenFeature(tf.int64):
          deep_input_from_raw_category.append(do_mask(deep_embed_matrix, features[k]))
        elif _ == tf.VarLenFeature(tf.float32):
          deep_input_from_raw_dense.append(features[k])
        else:
          raise ValueError('invalid features_spec value.')
      if FLAGS.use_features_of_all_type:
        deep_input = tf.concat(deep_input_from_raw_category + deep_input_from_raw_dense, axis = 1)
      elif FLAGS.use_features_of_category_type:
        deep_input = tf.concat(deep_input_from_raw_category, axis = 1)
      else:
        deep_input = tf.concat(deep_input_from_raw_dense, axis = 1)
  
  wide_input = None
  if wide_used:
    with vs.variable_scope('wide_input', partitioner = input_layer_partitioner, reuse = tf.AUTO_REUSE):
      wide_embed_matrix = tf.get_embedding_variable('wide_emb_matrix', embedding_dim = 1,
                                                    key_dtype = embed_key_type,
                                                    initializer = emb_initializer)
      wide_input_from_raw_category = []
      for k, _ in features_spec.items():
        k = str(k)
        if _ == tf.VarLenFeature(tf.int64):
          wide_input_from_raw_category.append(do_mask(wide_embed_matrix, features[k]))
        else:
          raise ValueError('invalid features_spec value.')
      wide_input = tf.concat(wide_input_from_raw_category, axis = 1)

  return wide_input, deep_input

def first_order_approximate(inputs):
  if inputs is None or len(inputs.get_shape()) != 3:
    raise ValueError('invalid input.')
  output = tf.reduce_sum(tf.reduce_sum(inputs, axis = 1, keepdims = True), axis = 2, keepdims = False)
  return output

def second_order_approximate(inputs):
  if inputs is None or len(inputs.get_shape()) != 3:
    raise ValueError('invalid input.')
  sum_squared = tf.square(tf.reduce_sum(inputs, axis = 1, keepdims = True))
  squared_sum = tf.reduce_sum(tf.square(inputs), axis = 1, keepdims = True)
  output = 0.5 * tf.reduce_sum(tf.subtract(sum_squared, squared_sum), axis = 2, keepdims = False)
  return output

def wide_net(wide_input,
             deep_input = None,
             function_approximate_order = 1):
  if wide_input is None:
    raise ValueError('wide input must be given.')
  with vs.variable_scope('first_order_logits'):
    _, w_mixed_dim = wide_input.get_shape()
    wide_input = tf.reshape(wide_input, shape=(-1, w_mixed_dim, 1))
    fst_order_output = first_order_approximate(wide_input)
  wide_output = fst_order_output
  if function_approximate_order == 2:
    with vs.variable_scope('second_order_logits'):
      _, d_mixed_dim = deep_input.get_shape()
      deep_input = tf.reshape(deep_input,
                              shape=(-1, (d_mixed_dim / FLAGS.embedding_dim), FLAGS.embedding_dim))
      if FLAGS.model_type == 'deepfm':
        snd_order_output = second_order_approximate(deep_input)
    wide_output += snd_order_output
  return wide_output

def deep_net(deep_input,
             hidden_size = (1024, 512),
             use_bn = False,
             kernel_initializer_type = 'he',
             activation_type = 'relu',
             mode = tf.estimator.ModeKeys.TRAIN):
  with vs.variable_scope('mlp'):
    layer_number = len(hidden_size)
    uniform = True if kernel_initializer_type == 'he' else False
    for i in range(layer_number):
      deep_input = tf.layers.dense(inputs = deep_input, name = 'fc_' + str(i),
                                   units = hidden_size[i], activation = None, use_bias = True,
                                   kernel_initializer = get_initializer(kernel_initializer_type,
                                                                        uniform = uniform))
      if use_bn:
        is_train = (mode == tf.estimator.ModeKeys.TRAIN)
        deep_input = tf.layers.batch_normalization(deep_input, training = is_train, momentum = 0.99999)
      deep_input = get_activation_func(activation_type)(deep_input)
    deep_output = deep_input
    return deep_output

def logits_combination(logits_list, combination_type = 'cancation'):
  if not combination_type in {'cancation'}:
    raise ValueError('invalid model_type.')
  if not logits_list:
    raise ValueError('empty logits_list.')
  output_logits = [logits_list[0]]
  for i in range(1, len(logits_list)):
    if combination_type == 'cancation':
      output_logits.append(logits_list[i])
    else:
      raise NotImplementedError("not supported combination_type yet.")
  mid_output_logits = tf.concat(output_logits, axis = 1)
  output_logits = tf.layers.dense(inputs = mid_output_logits, name = 'fused_logits',
                                  kernel_initializer = get_initializer('he', uniform = True),
                                  units = 2, activation = None)
  return mid_output_logits, output_logits

def get_optimizer(optimizer_type,
                  learning_rate,
                  use_lazy_update = False):
  if optimizer_type == 'adam':
    optimizer = tf.train.AdamOptimizer(learning_rate = learning_rate, beta1 = 0.9, beta2 = 0.99) \
                if not use_lazy_update else \
                tf.contrib.opt.LazyAdamOptimizer(learning_rate = learning_rate, beta1 = 0.9, beta2 = 0.99)
  elif optimizer_type == 'adagrad':
    optimizer = tf.train.AdagradOptimizer(learning_rate = learning_rate)
  elif optimizer_type == 'momentum':
    optimizer = tf.train.MomentumOptimizer(learning_rate = learning_rate, momentum = 0.95)
  elif optimizer_type == 'ftrl':
    optimizer = tf.train.FtrlOptimizer(learning_rate = learning_rate, l2_regularization_strength = 0.1)
  else:
    optimizer = GradientDescentOptimizer(learning_rate = learning_rate)
  return optimizer

def model_fn(features, labels, mode):
  if not FLAGS.model_type in {'wide_and_deep', 'deepfm'}:
    raise ValueError('invalid model_type.')
  wide_input, deep_input = \
    get_input_layer(features, embed_key_type = tf.int64,
                    wide_used = FLAGS.use_wide, deep_used = FLAGS.use_deep, mode = mode)
  logits_list = []
  if FLAGS.use_wide:
    with vs.variable_scope('wide_net', reuse = tf.AUTO_REUSE):
      wide_output = wide_net(wide_input, deep_input, FLAGS.wide_part_order)
      logits_list.append(wide_output)
  if FLAGS.use_deep:
    with vs.variable_scope('deep_net', reuse = tf.AUTO_REUSE):
      deep_output = deep_net(deep_input,
                             hidden_size = (1024, 512, 256, 128), use_bn = FLAGS.do_bn,
                             kernel_initializer_type = FLAGS.kernel_initializer_type,
                             activation_type = FLAGS.activation_type, mode = mode)
      logits_list.append(deep_output)
  with vs.variable_scope('logits', reuse = tf.AUTO_REUSE):
    mid_logits, logits = logits_combination(logits_list)
    y_preds = tf.nn.softmax(logits = logits)
  model_spec = None
  if mode != tf.estimator.ModeKeys.PREDICT:
    with vs.variable_scope('loss'):
      cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels = labels, logits = logits)
      loss = tf.reduce_mean(cross_entropy)
      if FLAGS.use_regularization:
        reg_loss = tf.add_n(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
        loss += reg_loss
    if mode == tf.estimator.ModeKeys.TRAIN:
      g_step = tf.train.get_global_step()
      train_op_list = []
      if FLAGS.use_deep:
        d_learning_rate = tf.train.exponential_decay(learning_rate = FLAGS.d_init_learning_rate,
                                                     global_step = g_step,
                                                     decay_steps = FLAGS.steps_per_decay,
                                                     decay_rate = FLAGS.decay_factor, staircase = True)
        d_optimizer = get_optimizer(FLAGS.deep_optimizer_type, d_learning_rate)
        d_net_var_set = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope = 'deep_net')
        d_input_var_set = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope = 'deep_input')
        logit_var_set = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope = 'logits')
        if FLAGS.debug:
          print('d_net_var_set is: ', d_net_var_set)
          print('d_input_var_set is: ', d_input_var_set)
          print('logit_var_set is: ', logit_var_set)
        d_var_set = d_input_var_set + d_net_var_set + logit_var_set
        if FLAGS.do_bn:
          update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
          with ops.control_dependencies(update_ops):
            gradients, variables = zip(*d_optimizer.compute_gradients(loss, var_list = d_var_set))
            train_op_list.append(update_ops)
        else:
          gradients, variables = zip(*d_optimizer.compute_gradients(loss, var_list = d_var_set))
        gradients, _ = tf.clip_by_global_norm(gradients, FLAGS.grad_clip_val)
        d_train_op = d_optimizer.apply_gradients(zip(gradients, variables))
        train_op_list.append(d_train_op)

      if FLAGS.use_wide:
        w_learning_rate = tf.train.exponential_decay(learning_rate = FLAGS.w_init_learning_rate,
                                                     global_step = g_step,
                                                     decay_steps = FLAGS.steps_per_decay,
                                                     decay_rate = FLAGS.decay_factor, staircase = True)
        w_optimizer = get_optimizer(FLAGS.wide_optimizer_type, w_learning_rate)
        w_net_var_set = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope = 'wide_net')
        w_input_var_set = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope = 'wide_input')
        logit_var_set = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope = 'logits')
        if FLAGS.debug:
          print('w_net_var_set is: ', w_net_var_set)
          print('w_input_var_set is: ', w_input_var_set)
          print('logit_var_set is: ', logit_var_set)
        w_var_set = w_input_var_set + w_net_var_set + logit_var_set
        gradients, variables = zip(*w_optimizer.compute_gradients(loss, var_list = w_var_set))
        gradients, _ = tf.clip_by_global_norm(gradients, FLAGS.grad_clip_val)
        w_train_op = w_optimizer.apply_gradients(zip(gradients, variables))
        train_op_list.append(w_train_op)

      group_train_op = control_flow_ops.group(*train_op_list)
      with ops.control_dependencies([group_train_op]):
        with ops.colocate_with(g_step):
          update_g_step = state_ops.assign_add(g_step, 1)
      learning_rate = d_learning_rate if FLAGS.use_deep else w_learning_rate
      model_spec = group_train_op, update_g_step, loss, learning_rate

    else:
      with vs.variable_scope('metrics'):
        y_preds_hat = y_preds[:,-1]
      model_spec = y_preds_hat, labels

  else:
    with vs.variable_scope('pred_outputs'):
      y_preds_hat = y_preds[:,-1]
    model_spec = y_preds_hat

  return model_spec

